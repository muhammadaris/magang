@extends('layouts.template')

@section('content')
<div class="col-md-12 col-sm-12">
    <div class="row">
      <div class="x_panel">
        <div class="x_title">
          <h2>Data peminjaman buku Siswa</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
              <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
          <div class="">
            <div class="page-title">
              <div class="title_left">

              </div>
                    <div class="clearfix"></div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3  profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <img class="img-responsive avatar-view" src="{{ asset('12345.jpg') }}" alt="Avatar" style="height: 150px; width:160px;">
                        </div>
                      </div>
                      <br>


                        <li>
                            Nama Penulis : {{ $book->author }}
                        </li>
                        <li>
                           Nama Penerbit: {{ $book->publisher }}
                        </li>
                        <li>
                            Tahun : {{ $book->year }}
                        </li>
                        <li>

                      </ul>
                      <br/>
                    </div>
                    <div class="col-md-9 col-sm-9 ">
                        <div class="table">
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr style="text-align: center:">
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Buku</th>
                                  <th>Pinjam</th>
                                  <th>kembali</th>
                                  <th>Denda</th>
                                </tr>
                              </thead>
                              <tbody>

                                @foreach ( $book->peminjam as $i => $item )
                                <tr style="text-align:  center:">
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $item->borosRef->name }}</td>
                                    <td>{{ $item->borosRef->title }}</td>
                                    <td>{{ $item->start }}</td>
                                    <td>{{ $item->return }}</td>
                                    <td>Rp. {{ $item->denda }}</td>
                                </div>
                            </td>
                                @endforeach
                        </tr>
                    </tbody>
                </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection

