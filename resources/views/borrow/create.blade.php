@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Tambah Data peminjaman buku</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <!-- start form for validation -->
        <form action="{{ route('borrow.store') }}" method="post">
            @csrf
            <div class="form-group col-md-6">
            <label for="siswa">Siswa </label>
            <select class="form-control" id="siswa" name="siswa">
                <option>chose...</option>
                @foreach ($siswa as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
            <div class="form-group col-md-6">
            <label for="books">buku</label>
            <select class="form-control" id="books" name="books">
                <option>chose...</option>
                @foreach ($books as $item)
                <option value="{{ $item->id }}">{{ $item->title }}</option>
                @endforeach
            </select>


            </div>

            <div class="form-group col-md-6">
                <label for="tanggal_lahir">Tanggal pinjam</label>
                <input type="date" name="start" class="form-control"
                id="start">
            </div>
           <br>
            <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection
