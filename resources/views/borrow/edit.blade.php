@extends('layouts.template')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Ubah Data peminjaman buku</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <!-- start form for validation -->
        <form action="{{ route('borrow.update', $borrow->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group col-md-12">
            <label for="siswa"> Nama Siswa </label>
            <select class="form-control" id="siswa" name="siswa">
                <option>chose...</option>
                @foreach ($siswa as $item)
                <option value="{{ $item->id }}" {{ $borrow->siswa_id==
                $item->id ? 'selected' :'' }}> {{ $item->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
            <div class="form-group col-md-12">
            <label for="books">Judul Buku</label>
            <select class="form-control" id="books" name="books">
                <option>chose...</option>
                @foreach ($books as $item)
                <option value="{{ $item->id }}" {{ $borrow->book_id==
                    $item->id? 'selected' :'' }}>{{ $item->title }}</option>
                @endforeach
            </select>
            </div>
            <div class="form-group col-md-12">
                <label for="date">Tanggal pinjam</label>
                <input type="date" class="form-control" id="start"
                name="start" value="{{ $borrow->start }}">
            </div>
            <div class="form-group col-md-12">
                <label for="date">tanggal kembali</label></small></label>
                <input type="date" class="form-control" id="return"
                name="return" placeholder="Masukkan tanggal kembali">
            </div>
            <label class="mr-4">status</label>
            <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="status"
                id="status1" value="dipinjam" class="custom-control-input" {{ $borrow->status == 'dipinjam' ? 'checked' : '' }}>
                <label class="custom-control-label" for="status1">dipinjam</label>
                </div>
            </div>

            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" name="status"
                id="status2" value="dikembalikan" class="custom-control-input"  {{ $borrow->status == 'dikembalikan' ? 'checked' : '' }}>
                <label class="custom-control-label" for="status2">dikembalikan</label>
                </div>
                <div class="form-group col-md-12">
                    <label for="denda">Denda</label>
                    <input type="text" class="form-control" id="denda"
                    name="denda" readonly>
                </div>
            </div>
           <br>
            <button type="submit" class="btn btn-danger">Simpan</button>
        </form>
    <!-- end form for validations -->
    </div>
</div>
@endsection

@push('script')
<script>
    $('#start, #return').change(()=>{
        var pinjam = $('#start').val()
        var kembali = $('#return').val()
        //cek jika input pinjam dan kembali tidak kosong
        if(Date.parse(pinjam) && Date.parse(kembali) )
        {
            pinjam = new Date(pinjam)
            kembali = new Date(kembali)

            //membuat tanggal deadline
            var deadline = pinjam.setDate(pinjam.getDate() + 7)

            //menghitung selisih hari dan denda
            var selisih  =(kembali - deadline) / (1000 * 3600 * 24)
            var denda = selisih > 0 ? (selisih*500) : 0;

            $('#denda').val(denda)

            //membuat fungsi data tanggal deadline untuk ditampilkan
            var convertDeadline = new Date (deadline)
            var formattedDate = ('0' + convertDeadline.getDate()).slice(-2);
            var formattedMonth = ('0' + (convertDeadline.getMonth() + 1)).slice(-2);
            var formattedYear =  convertDeadline.getfullYear(). toString().substr(2,2);
            var dateString = formattedMonth + '/' + formattedDate + '/' + formattedYear;

            //menampilkan deadline
            $('#kembali').text( '(Deadline : '+dateString+')')

        }else{
            $('#kembali').text( '(Deadline : -)')
            $('#denda').val(0)
        }
    })
</script>
@endpush
